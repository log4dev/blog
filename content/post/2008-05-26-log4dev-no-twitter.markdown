---
author: mgalves
categories:
- Notícias
comments: true
date: "2008-05-26T08:53:54Z"
slug: log4dev-no-twitter
theme:
  name: twitter
title: Log4Dev no Twitter
wordpress_id: 303
---

Log4Dev ficou com ciúmes do Job4Dev e pediu uma conta no Twitter também. E ela existe!

[http://twitter.com/log4dev ](http://twitter.com/log4dev )

Confesso que estou gostando do conceito de microblog (ou uma espécie de chat aberto assíncrono) do Twitter.
