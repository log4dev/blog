---
author: mgalves
comments: true
date: "2013-04-15T00:00:00Z"
description: ""
slug: devops-noops-afinal-que-raios--isso
tags:
- DevOps
- Desenvolvimento
- Engenharia de Software
theme:
  name: twitter
title: DevOps, NoOps. Afinal, que raios é isso?
---

Estou como autor deste post, mas na verdade o autor deveria ser o nosso colega Thiago Ganzarolli, que é o real autor da apresentação sobre DevOps abaixo. Achei propício, tendo em vista o tema abordado no meu [último post]({{< ref 2013-04-11-vagrant--chef >}}).


{{< slideshare title="DevOps, NoOps...afinal que raios é isso?" url="tganzarolli/devops-noopsafinal-que-raios-isso" embed_code=15141885 >}}
