---
author: mgalves
categories:
- Desenvolvimento
comments: true
date: "2006-10-03T14:44:51Z"
slug: lambda-probe
tags:
- Ferramentas
- Java
- Web
theme:
  name: twitter
title: Lambda Probe
wordpress_id: 86
---

Pra quem desenvolve ou monitora aplicativos rodando em Tomcat/JBoss, a ferramenta LambdaProbe pode ser de grande valia. Este aplicativo web, que roda dentro do próprio servidor de aplicações, fornece uma interface web bastante intuitiva fornecendo uma grande quantidade de informações úteis. Dentre elas, a lista de aplicativos instalados e ativos no servidor, número de sessões por aplicativos, objetos salvos na sessão, datasources, memória utilizada e acesso aos logs de servidor. Além disso, permite executar deploy, undeploy, start e stop de cada aplicativo. Pode ser instalado a partir do site [www.lambdaprobe.org](http://www.lambdaprobe.org/)
