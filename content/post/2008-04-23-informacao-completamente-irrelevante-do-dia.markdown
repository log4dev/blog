---
author: lullis
comments: true
date: "2008-04-23T15:09:08Z"
slug: informacao-completamente-irrelevante-do-dia
theme:
  name: twitter
title: Informação completamente irrelevante do dia…
wordpress_id: 279
---

Se o editor-chefe resolvesse hospedar este blog usando o [Amazon Web Services](http://developer.amazonwebservices.com/connect/ann.jspa?annID=313), o custo mensal de transferência seria de R$0,46.
