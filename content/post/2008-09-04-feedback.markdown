---
author: mgalves
categories:
- Notícias
- Opinião
comments: true
date: "2008-09-04T10:38:34Z"
slug: feedback
tags:
- Ferramentas
- job4dev
- Mercado
theme:
  name: twitter
title: Feedback
wordpress_id: 351
---

Email enviado por Daniel Cassiano ([http://danielcassiano.net/](http://danielcassiano.net/)) na segunda-feira, 2 de Setembro:


> Olá!

Gostaria de levar a conhecimento de vocês que consegui meu trabalho através do job4dev!
Na verdade isso já tem 3 meses e meio. Estava para fazer isso há um tempo, mas sempre algo impedia.
Hoje estou aqui pra expressar minha gratidão a vocês! Muito obrigado!

Empresa que me contratou: Apontador.com (atualmente Apontador MapLink)

Parabés por essa iniciativa.
Vou publicar a gif de vocês no meu site.

Abs!


Não sei quanto a vocês, mas este tipo de feedback só faz aumentar a minha motivação e a minha convicção de que este projeto está no caminho certo.
