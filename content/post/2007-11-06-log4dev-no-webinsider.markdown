---
author: mgalves
categories:
- Notícias
comments: true
date: "2007-11-06T15:09:05Z"
slug: log4dev-no-webinsider
tags:
- Web
theme:
  name: twitter
title: Log4Dev no Webinsider
wordpress_id: 205
---

Atenção leitores deste blog. O Raphael está virando POP!

O artigo [PC 2.0](http://log4dev.com/2007/11/06/pc-20/) acabou de ser publicado no Webinsider.

[http://webinsider.uol.com.br/index.php/2007/11/06/android-e-o-pc-20-a-versao-mobile-do-ibm-pc/](http://webinsider.uol.com.br/index.php/2007/11/06/android-e-o-pc-20-a-versao-mobile-do-ibm-pc/)

Parabéns.
