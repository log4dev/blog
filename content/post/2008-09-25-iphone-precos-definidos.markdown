---
author: mgalves
categories:
- Negocios
- Notícias
comments: true
date: "2008-09-25T20:12:11Z"
slug: iphone-precos-definidos
tags:
- Apple
- iPhone
- Mercado
theme:
  name: twitter
title: 'iPhone: preços definidos.'
wordpress_id: 355
---

A Folha confirmou: Vivo e Claro lançam no dia 26 (vulgo amanhã).

Preços: de [1000 a 3000 na Claro](http://www1.folha.uol.com.br/folha/informatica/ult124u448784.shtml), e de 899 a 2199 na [Vivo](http://www1.folha.uol.com.br/folha/informatica/ult124u448974.shtml).

Acho que não será agora que terei meu iPhone. Muito caro!
