---
author: mgalves
categories:
- Notícias
comments: true
date: "2008-10-24T09:01:24Z"
slug: no-twitter-again
tags:
- Twitter
theme:
  name: twitter
title: No Twitter, again
wordpress_id: 364
---

Post rápido, apenas para anunciar que o usuário [@log4dev](http://twitter.com/log4dev) no Twitter foi reativado. Na prática isto significa que novos textos serão divulgados na plataforma, e eventualmente links e curiosidades.
