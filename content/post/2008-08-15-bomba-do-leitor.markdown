---
author: mgalves
categories:
- Desenvolvimento
comments: true
date: "2008-08-15T15:10:46Z"
slug: bomba-do-leitor
tags:
- Google
- Web
theme:
  name: twitter
title: Bomba do leitor
wordpress_id: 345
---

Plagiando a sessão da revista Mac+ mostro aqui um "bug" enviado por Roberto Endo (também conhecido como o rendo). O bug não é de um produto da Apple, mas sim do Google.

[![bug_google.gif](http://beta.log4dev.com/wp-content/uploads/2008/08/bug_google.gif)](http://beta.log4dev.com/wp-content/uploads/2008/08/bug_google.gif)

Os usuários assíduos dos produtos do Google reconhecerão na imagem acima um pedaço da tela de configuração. O interessante é que uma dada sequência de comandos (que o Roberto não revelou) faz aparecer a linha de javascript na barra inferior do browser. Não sei vocês, mas eu achei no mínimo curioso o if(0 != 1)....
