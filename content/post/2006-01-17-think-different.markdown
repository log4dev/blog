---
author: mgalves
categories:
- Negocios
comments: true
date: "2006-01-17T11:03:11Z"
slug: think-different
tags:
- Apple
theme:
  name: twitter
title: Think Different
wordpress_id: 37
---

Lendo alguns artigos sobre os novos Macs, caí num artigo falando sobre spots comerciais da Apple. Graças às maravilhas do Hipertexto (também conhecido como link em uma webpage), descobri um site com muito material sobre a Apple...posteres (ou será posters ?), objetos, fotos, e vídeos. (os vídeos podem ser encontratos em [The Apple Collection Quicktime Movies @ The Apple Collection](http://www.theapplecollection.com/Collection/AppleMovies/)Entre os vídeo, dois me chamaram a atenção: um da campanha [Think Different](http://www.theapplecollection.com/Collection/AppleMovies/mov/thinkdif.html), e um lançado em [1984](http://www.theapplecollection.com/Collection/AppleMovies/mov/1984_big.html), sobre os novos Macs. Vale a pena dar uma olhada.
