---
author: mgalves
comments: true
date: "2007-10-25T09:33:09Z"
slug: painel-de-controle-do-mac-os-x
tags:
- Apple
- Interfaces
- Mac OS
theme:
  name: twitter
title: Painel de controle do Mac OS X
wordpress_id: 200
---

Recebi  este [link interessante](http://www.appleinsider.com/articles/07/10/24/road_to_mac_os_x_leopard_system_preferences.html&page=1) do AppleInsider mostrando a evolução dos painéis de controle do Mac OS, desde a versão 3 (1986) até o novíssimo Leopard (10.5), que está para ser lançado.  Para saudosistas, macmaníacos e interessados por interfaces gráficas e IHM.
