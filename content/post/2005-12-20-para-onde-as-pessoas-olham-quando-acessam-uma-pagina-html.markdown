---
author: mgalves
comments: true
date: "2005-12-20T11:23:00Z"
slug: para-onde-as-pessoas-olham-quando-acessam-uma-pagina-html
tags:
- Interfaces
- Pesquisa
- Web
theme:
  name: twitter
title: Para onde as pessoas olham quando acessam uma página HTML
wordpress_id: 18
---

Entender o comportamento dos internautas é essencial para o sucesso de uma página na web.

O artigo [**Eyetrack III: What News Websites Look Like Through Readers' Eyes **](http://www.poynter.org/content/content_view.asp?id=70472)da uma
idéia de alguns estudos sobre o comportamento de leitura de pessoas visitando sites de notícias.

