---
author: mgalves
categories:
- Humor
comments: true
date: "2008-04-14T17:17:46Z"
slug: dia-a-dia-do-programador
theme:
  name: twitter
title: Dia a dia do programador
wordpress_id: 273
---

Na série "Uma imagem que vale por mil palavras"

![](/images/2008-04-14-dia-a-dia-do-programador/programmer-job.png)

Extraído de [/images/2008-04-14-dia-a-dia-do-programador/programmer-job.png](/images/2008-04-14-dia-a-dia-do-programador/programmer-job.png)

Como eu sempre digo, 95% dos bugs terminam com um "Ahhhhhhhhhhhhhhhhhh...", seguido da constatação de que o problema foi criado pela interface cadeira-teclado.
