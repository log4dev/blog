---
author: mgalves
comments: true
date: "2007-08-03T10:14:08Z"
slug: curso-online-de-javascript
tags:
- Javascript
- Linguagens de Programação
theme:
  name: twitter
title: Curso online de Javascript
wordpress_id: 159
---

Quem estiver procurando um **curso** minimamente estruturado de **Javascript**, que aborde conceitos interessantes da linguagem e não apenas formas de manipulação de DOM, aconselho este site: [http://eloquentjavascript.net/.](http://eloquentjavascript.net/)
Algo que me chamou a atenção foi o fato do curso abordar tópicos de cursos básicos de teoria de computação (raramente abordados quando se fala em Javascript)  como **estruturas de dados**, **heaps**, **árvores binárias**, **algoritmos de busca** e **expressões regulares**.  Este texto se encaixa na teoria de que [Javascript é uma linguagem real de programação](http://log4dev.wordpress.com/2007/07/12/javascript-e-uma-linguagem-seria/), e não apenas uma ferramentinha pra deixar páginas web mais dinâmicas.
