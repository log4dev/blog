---
author: mgalves
comments: true
date: "2006-01-10T09:58:41Z"
slug: down-them-all
tags:
- Ferramentas
- Interfaces
- Web
theme:
  name: twitter
title: Down them All !!!!!!!
wordpress_id: 31
---

O plugin [downthemall](http://downthemall.mozdev.org/)  para Firefox 1.5 é uma mão na roda para aquelas páginas com um monte de links para fotos ou arquivos. Ele simplesmente puxa todos os arquivos desejados de uma vez.
