---
author: mgalves
categories:
- Ferramentas
- Idéias
comments: true
date: "2008-12-11T08:56:20Z"
slug: 1-ano-de-job4dev
tags:
- Internet
- job4dev
- Mercado
- Web
theme:
  name: twitter
title: 1 ano de job4dev
wordpress_id: 376
---

No dia 3 de dezembro, o job4dev fez um ano de vida! Ok, eu sou um pai/criador meio desnaturado, porque confesso que apenas me lembrei disso agora. Mas antes tarde do que nunca.

Este post é apenas para lembrar que estes 12 meses me comprovaram que é possível oferecer uma alternativa interessante aos sites medíocres que existem por aí, que existe sim um mercado interessante em busca de profissionais interessantes, e, sobretudo, que existe público para um site desses.

E tendo isto em mente, vamos continuando o trabalho. Inclusive, se tudo der certo e o Raphael não desistir de sua empreitada, em breve teremos um major upgrade do sistema. Aguardem!
