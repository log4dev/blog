---
author: mgalves
categories:
- Desenvolvimento
comments: true
date: "2006-01-03T18:26:00Z"
slug: teste-simples-de-qualidade-de-desenvolvimento
tags:
- Qualidade de Software
theme:
  name: twitter
title: Teste simples de qualidade de desenvolvimento
wordpress_id: 28
---

O artigo   [The Joel Test: 12 Steps to Better Code](http://www.joelonsoftware.com/articles/fog0000000043.html) propõe um pequeno teste, transcrito abaixo, para verificar a qualidade do seu ambiente de desenvolvimento.Apesar de simples, aborda pontos importantes para qualquer ambiente de desenvolvimento, e parece um bom ponto de partida para uma avaliação básica.

1. Do you use source control?
2. Can you make a build in one step?
3. Do you make daily builds?
4. Do you have a bug database?
5. Do you fix bugs before writing new code?
6. Do you have an up-to-date schedule?
7. Do you have a spec?
8. Do programmers have quiet working conditions?
9. Do you use the best tools money can buy?
10. Do you have testers?
11. Do new candidates write code during their interview?
12. Do you do hallway usability testing?


