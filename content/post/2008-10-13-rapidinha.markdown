---
author: mgalves
categories:
- Desenvolvimento
comments: true
date: "2008-10-13T10:19:51Z"
slug: rapidinha
tags:
- Ajax
- Django
- Javascript
- Python
- Web
theme:
  name: twitter
title: Rapidinha
wordpress_id: 361
---

Só pra falar algo que talvez já seja óbvio pro pessoal mais esperto: o trio Python/Django/Prototype (Scriptaculous) tem um poder incrível de ajudar a desenvolver coisas úteis, eficientes e bonitas em pouquíssimo tempo. A minha produtividade com esta trinca tem sido muito grande, fator essencial para a evolução dos meus projetos pessoais ([job4dev](http://job4dev.com) e [sigaseutime](http://www.sigaseutime.com.br)).
