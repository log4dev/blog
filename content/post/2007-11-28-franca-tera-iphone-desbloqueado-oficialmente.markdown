---
author: mgalves
categories:
- Notícias
comments: true
date: "2007-11-28T14:45:05Z"
slug: franca-tera-iphone-desbloqueado-oficialmente
tags:
- Apple
- Mercado
- Mobile
theme:
  name: twitter
title: França terá iPhone desbloqueado oficialmente
wordpress_id: 215
---

Saiu no [Apple Insider](http://www.appleinsider.com/articles/07/11/28/apple_prices_unlocked_iphones_at_749_euros_in_france.html):



> Apple and wireless partner Orange will launch iPhone in France later today, offering customers three distinct pricing options for the handset, including a fully unlocked and carrier independent version for 749 euros ($1105).



A lei francesa obriga empresas a oferecerem uma contrapartida aberta a qualquer produto que seja bloqueado ou atrelado a algum serviço. O preço é alto, mas pessoalmente prefiro pensar em pagar mais caro pra ter um desbloqueio oficial do que ter que ficar dependendo de artimanhas que podem ser quebradas por atualizações e afins.

Quem sabe....
