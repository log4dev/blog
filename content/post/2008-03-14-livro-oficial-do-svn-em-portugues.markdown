---
author: mgalves
categories:
- Desenvolvimento
- Notícias
comments: true
date: "2008-03-14T09:40:29Z"
slug: livro-oficial-do-svn-em-portugues
tags:
- Comunidade
- Desenvolvimento
- Ferramentas
- SVN
theme:
  name: twitter
title: Livro oficial do SVN em Português
wordpress_id: 262
---

Existe no Google Code o projeto [**svnbook-pt-br**](http://code.google.com/p/svnbook-pt-br/) para a tradução do livro oficial do SVN, Version Control with [Subversion (http://svnbook.red-bean.com/)](http://svnbook.red-bean.com/). Existem versões parciais disponíveis para download e o projeto aceita colaboradores.
