---
author: mgalves
categories:
- Notícias
comments: true
date: "2008-04-21T23:29:40Z"
slug: job4dev-twitter
tags:
- job4dev
- Twitter
- Web
theme:
  name: twitter
title: Job4Dev + Twitter
wordpress_id: 276
---

Na sexta feira entrou no ar uma nova funcionalidade do [Job4Dev](http://job4dev.com): integração com o [Twitter](http://twitter.com). Para quem não conhece o Twitter, acho que a melhor explicação que eu posso dar é o texto da página inicial do próprio site:


> Twitter is a service for friends, family, and co–workers to communicate and stay connected through the exchange of quick, frequent answers to one simple question: What are you doing?


Resumindo tudo: é um microblog. E qual o interesse do Job4Dev por um microblog? Simples: mais formas de divulgação. O Twitter permite o envio de mensagens através de Google Talk, Jabber, Live Journal, RSS e SMS, além do próprio site.

Temos agora um usuário job4dev no twitter, que pode ser acessado através da URL [http://twitter.com/job4dev](http://twitter.com/job4dev): para onde todos os anúncios de vagas aprovados serão enviados a cada meia-hora, e divulgadas em tempo real para todos os usuários do Twitter que cadastrados na nossa rede de contatos. Simples e eficiente!

_PS: Gostaria de aproveitar para dizer que o RSS do Job4Dev no Google Reader anda meio problemático ele não é atualizado desde o início de abril. Estamos tentando corrigir isso o quanto antes. _
