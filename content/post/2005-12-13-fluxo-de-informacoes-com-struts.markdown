---
author: mgalves
categories:
- Desenvolvimento
comments: true
date: "2005-12-13T14:20:00Z"
slug: fluxo-de-informacoes-com-struts
tags:
- Java
- Web
theme:
  name: twitter
title: Fluxo de Informações com Struts
wordpress_id: 11
---

Dica interessante para novatos (e talvez não tão novatos assim) em **Struts**: O link abaixo descreve diferentes **fluxos de  processamento** de uma aplicação, e como implementa-los usando **mapeamento de ações em Struts**.

[http://www.theserverside.com/articles/article.tss?l=StrutsActionMapping](http://www.theserverside.com/tt/articles/article.tss?l=StrutsActionMapping)

Vale a pena dar uma olhada.
