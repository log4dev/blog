---
author: mgalves
categories:
- Ferramentas
- Produto
comments: true
date: "2008-10-28T08:53:45Z"
slug: sliderocket
tags:
- Adobe
- Ferramentas
- Flex
- Slideshow
- Web
theme:
  name: twitter
title: SlideRocket
wordpress_id: 366
---

Uma ótima alternativa para aqueles que precisam criar apresentações, não são chegados em PowerPoint, querem ter a possibilidade de usar a web como plataforma e querem fugir dos tentáculos do Google: [SlideRocket (www.sliderocket.com). ](http://www.sliderocket.com)

Baseado na tecnologia [Adobe Flex](http://www.adobe.com/products/flex/) e [Adobe AIR](http://www.adobe.com/products/air/), o **SlideRocket** permite criar, compartilhar e exibir apresentações online. Os pontos fortes do sistema são a interface de criação de slides, a qualidade do resultado e a possibilidade de exibir online (em modo fullscreen) ou de criar um executável para o Desktop com o AIR.

Tive a oportunidade de ver uma apresentação utilizando esta ferramenta, e achei os resultados surpreendentes. Segundo um amigo marketeiro e criador constante de apresentações, apesar de alguns bugs a facilidade de uso vale a pena. Em alguns casos, como incluir vídeos em Flash, o software  é inclusive superior ao Powerpoint. 

O sistema oferece 3 opções de  planos. O plano gratuíto oferece 250MB de espaço, ferramentas de criação e número ilimitado de apresentações (desde que não ultrapassem os 250MB, obviamente...). Os planos pagos oferecem, entre outras coisas, mais espaço e possibilidade de controle de versão.
