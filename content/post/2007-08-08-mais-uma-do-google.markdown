---
author: mgalves
comments: true
date: "2007-08-08T09:56:42Z"
slug: mais-uma-do-google
tags:
- Formação
- Google
theme:
  name: twitter
title: Mais uma do Google
wordpress_id: 163
---

Acabei de descobrir o site [Google Code for Educators](http://code.google.com/edu/), que contém uma série de tutoriais sobre tecnologia. Dei uma olhada no curso de AJAX, parece ser bem feito. Além disso, o sistema oferece alguns vídeos de palestras e um sistema de busca especializado em material didático e científico.
