---
author: mgalves
categories: null
comments: true
date: "2005-12-28T16:44:44Z"
slug: filosofia-de-trabalho-aprendendo-com-open-source
tags:
- Software Livre
theme:
  name: twitter
title: Filosofia de trabalho - Aprendendo com Open Source
wordpress_id: 23
---

O texto [What Business Can Learn from Open Source](http://www.paulgraham.com/opensource.html) discute como é possível que projetos Open Source, desenvolvidos por programadores do mundo todo (em geral trabalhando de graça), podem competir em pé de igualdade com programas desenvolvidos por multinacionais (que investem milhões), e como isso pode ser utilizado como filosofia de trabalho para empresas e pessoas em geral.


> 
