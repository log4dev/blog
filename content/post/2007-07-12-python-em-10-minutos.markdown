---
author: mgalves
comments: true
date: "2007-07-12T20:18:31Z"
slug: python-em-10-minutos
tags:
- Python
theme:
  name: twitter
title: Python em 10 minutos
wordpress_id: 147
---

Recebi hoje o link deste **tutorial **de **Python**, [Learn Python in 10 minutes](http://www.poromenos.org/tutorials/python). Dei uma rápida olhada, e parece ser bem útil, um bom kick-off.

PS: Isso me lembra quando li um tutorial de Perl em 10 minutos para começar a escrever um etiquetador de palavras. Mas isto é outra história...
