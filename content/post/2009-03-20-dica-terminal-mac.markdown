---
author: mgalves
comments: true
date: "2009-03-20T21:00:20Z"
slug: dica-terminal-mac
tags:
- Apple
- Ferramentas
- Mac
theme:
  name: twitter
title: 'Dica Terminal / Mac '
wordpress_id: 490
---

Descobri um comando hoje que pode ser útil em scripts bash: é possível acessar a àrea de transferência do OS X através do bash usando o comando pbcopy.

Por exemplo, para o conteúdo de um arquivo, basta fazer: > cat arquivo | pbcopy

Nessas horas eu vejo que eu não sei picas do Darwin...
