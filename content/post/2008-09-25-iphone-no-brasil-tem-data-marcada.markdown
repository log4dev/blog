---
author: mgalves
categories:
- Notícias
- Produto
comments: true
date: "2008-09-25T09:26:34Z"
slug: iphone-no-brasil-tem-data-marcada
tags:
- Apple
- iPhone
- Mercado
theme:
  name: twitter
title: iPhone no Brasil tem data marcada
wordpress_id: 354
---

Rápido e rasteiro: o **iPhone**, segundo fontes próximas às negociações, agora tem data oficial para ser lançado no Brasil, segundo o site [MacWorld Brasil](http://www.macworldbrasil.com.br/noticias/2008/09/16/lancamento-do-iphone-3g-no-brasil-esta-marcado-para-26-de-setembro/)!

Será no dia **26 de setembro,** em evento conjunto da **Apple**, **Claro** e **Vivo**. O telefone será lançado também na Turquia neste dia.

Mas a pergunta quer não quer calar: quanto vai custar o bendito?! Isso nem Jobs deve saber......
