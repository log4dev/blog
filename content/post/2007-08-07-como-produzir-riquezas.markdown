---
author: lullis
categories:
- Negocios
comments: true
date: "2007-08-07T03:14:33Z"
slug: como-produzir-riquezas
tags:
- Startup
theme:
  name: twitter
title: Como Produzir Riquezas
wordpress_id: 162
---

Meu último texto foi recebido como um bebê de fraldas sujas pelos leitores. A maior parte olhou com cuidado, disse "que bonitinho" e depois ficou sem saber o que fazer.

Afirmar algo como "agora você pode tomar as rédeas da sua vida produtiva" pode causar isso. A reação natural é se perguntar "Ok. Mas como é que o meu computador e minha conexão com a internet vão me ajudar a pagar o meu aluguel e meu aparelho ortodôntico?".

Qualquer pessoa que te falar que tem uma receita infalível para ganhar dinheiro ou é louca, ou é vigarista.

O difícil é conseguir explicar de uma forma direta que _riqueza e dinheiro não são a mesma coisa. _E, levando em conta que eu tenho passado a maior parte do meu tempo trabalhando nos meus projetinhos (por enquanto) secretos, não vou tentar explicar isso melhor.

Vou usar um [artigo de Paul Graham](http://lullis.infogami.com/pg/Wealth) para isso.
