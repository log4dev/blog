---
author: mgalves
categories:
- Notícias
comments: true
date: "2008-04-16T16:03:59Z"
slug: combr-liberado-para-cpf
tags:
- Web
theme:
  name: twitter
title: .com.br liberado para CPF
wordpress_id: 274
---

Até pouco tempo atrás, para se registrar um domínio **.com.br**, era necessário possuir um CNPJ. Ou seja, apenas donos de empresa podiam registrar domínios **.com.br**. O resultado é que das duas uma: ou pessoas físicas compravam domínios **.com**, ou utilizavam serviços de empresas que compravam o domínio em seu nome.

Mas isto mudou. Acabei de receber o seguinte email do [registro.br](http://registro.br), orgão responsável pelo controle dos domínios brasileiros:


> Por decisão do CGI.br, o domínio [COM.BR](http://com.br/), destinado a atividades comerciais genéricas na Internet, também poderá ser registrado sob um CPF. Ou seja, pessoas naturais com atividades comerciais e afins poderão registrar domínios [COM.BR](http://com.br/).

Esta modificação terá efeito a partir do dia 01/05/2008.

Inicialmente, somente o domínio [COM.BR](http://com.br/) estará disponível nesta nova categoria, genérica, que permite registro tanto com CNPJ quanto com CPF. Lembramos que, para manter a transparência do registro de domínios .br, pessoas físicas responsáveis por domínios [COM.BR](http://com.br/) estarão sujeitas aos mesmos procedimentos das entidades cadastradas previamente.
