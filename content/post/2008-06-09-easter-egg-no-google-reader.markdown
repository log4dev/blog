---
author: mgalves
categories:
- Humor
comments: true
date: "2008-06-09T15:34:31Z"
slug: easter-egg-no-google-reader
tags:
- Ferramentas
- Google
- Web
theme:
  name: twitter
title: Easter Egg no Google Reader
wordpress_id: 312
---

Entre no Reader, digite a seguinte sequencia no teclado:

**cima, cima, baixo, baixo, esquerda, direita, esquerda, direita, b, a **

A palavra Ninja! irá aparece na busca, a barra a esquerda irá ter um skin diferente e todos os contadores ficarão com valor 30 (mensagens não lidas). A referência nerd da coisa (obrigado Thomas)  é que existe um jogo chamado Contra, do Nintendinho, onde esta sequência dá ao jogador 30 vidas. Esse código também é chamado de Konami Code, e é bem documentado pela Wikipedia em [http://en.wikipedia.org/wiki/Konami_Code](http://en.wikipedia.org/wiki/Konami_Code).

Mais um daqueles códigos que o pessoal dava a vida para conseguir, e que eu sempre fui incapaz de aprender de cor!
