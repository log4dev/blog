---
author: mgalves
comments: true
date: "2007-04-10T16:56:29Z"
slug: palm-inc-adota-linux
tags:
- Ferramentas
- Hardware
- Linux
- Palm
theme:
  name: twitter
title: Palm Inc adota Linux
wordpress_id: 123
---

Segundo o [Palm Info Center](http://www.palminfocenter.com/news/9351/palm-announces-new-linux-based-mobile-platform/), a Palm anunciou que irá lançar uma plataforma móvel baseada em Linux rodando entre outras coisas o navegador Opera. Depois do anuncio da separação da Palm e Palm Source (que produz o SO), parece que finalmente as coisas estão tomando um rumo decente...
