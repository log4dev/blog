---
author: mgalves
categories:
- Desenvolvimento
- Teoria
comments: true
date: "2007-12-04T20:00:58Z"
slug: por-dentro-das-expressoes-regulares
tags:
- Algoritmos
- Desenvolvimento
- Regexp
- Teoria
theme:
  name: twitter
title: Por dentro das Expressões Regulares
wordpress_id: 220
---

Aproveitando os dois últimos posts sobre expressões regulares, [Regexp Nossa de Cada Dia!](http://log4dev.com/2007/11/19/regexp-nossa-de-cada-dia/) (de minha autoria) e [Usando Regexp](http://log4dev.com/2007/12/02/usando-regexp/) (do Leo), encontrei  o link do artigo "[How Regexes Work](http://perl.plover.com/Regex/article.html)", que explica de forma bem clara o funcionamento de um algoritmo de regexp. Bom para saciar a curiosidade daqueles que gostam de saber como as coisas funcionam.
