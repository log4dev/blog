---
author: mgalves
categories:
- Desenvolvimento
comments: true
date: "2005-12-26T14:14:00Z"
slug: dicas-sobre-struts
tags:
- Ferramentas
- Java
- Web
theme:
  name: twitter
title: Dicas sobre Struts
wordpress_id: 21
---

O site [Husted.com - About Struts](http://www.husted.com/struts/) contém um conjunto interessante de **dicas** e **artigos **sobre **Struts**. Boa fonte de informações para programadores de vários níveis.




PS: Feliz Natal.
