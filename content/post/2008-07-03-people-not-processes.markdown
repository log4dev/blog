---
author: lullis
comments: true
date: "2008-07-03T18:21:25Z"
slug: people-not-processes
theme:
  name: twitter
title: People, not process
wordpress_id: 329
---

Não é a primeira vez que recorro ao cinema para passar a mensagem.


> “I never think about the audience. If someone gives me a marketing report, I throw it away.”


Andrew Stanton, criador de Wall-E
