---
author: mgalves
categories:
- Desenvolvimento
- Design
comments: true
date: "2008-05-08T10:18:49Z"
slug: frase-do-dia-5
theme:
  name: twitter
title: Frase do Dia
wordpress_id: 295
---

_"Customers don't know what they want, much less what they need, until they see it."_
