---
author: mgalves
categories:
- Desenvolvimento
comments: true
date: "2008-01-23T12:27:00Z"
slug: usuario-de-django
tags:
- Django
- Python
- Web
theme:
  name: twitter
title: Usuário de Django?
wordpress_id: 234
---

Encontre os programadores usuários de Django ao redor do mundo. Quem sabe não tem um na rua de baixo?

[http://djangopeople.net/](http://djangopeople.net/)
