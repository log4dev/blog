---
author: mgalves
categories:
- Desenvolvimento
comments: true
date: "2006-01-15T22:47:00Z"
slug: primeiros-passos-em-python
tags:
- Python
- Linguagens de Programação
theme:
  name: twitter
title: Primeiros passos em Python
wordpress_id: 35
---

Recentemente comecei a ter um grande interesse por Python. Ainda não escrevi nenhuma linha de código...mas isso é questão de tempo. Por enquanto estou apenas lendo vário artigos sobre.

No site python.org, existe uma boa documentação sobre a linguem, artigos, etc...

Aconselho o artigo [Why Python,](http://www.linuxjournal.com/article/3882) e um conjunto de artigos comparando Python com outras linguagens. Segue o [link](http://wiki.python.org/moin/LanguageComparisons).
