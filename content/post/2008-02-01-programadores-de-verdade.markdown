---
author: mgalves
categories:
- Desenvolvimento
- Humor
comments: true
date: "2008-02-01T08:02:07Z"
slug: programadores-de-verdade
theme:
  name: twitter
title: Programadores de verdade
wordpress_id: 242
---

[![](/images/2008-02-01-programadores-de-verdade/real_programmers.png)](/images/2008-02-01-programadores-de-verdade/real_programmers.png)

Do sempre genial XKCD ([/images/2008-02-01-programadores-de-verdade/real_programmers.png](/images/2008-02-01-programadores-de-verdade/real_programmers.png))
_PS: isto me lembra uma palestra em um curso da UNICAMP sobre segurança onde um dos alunos, que iria falar sobre segurança da JVM, disse que uma das únicas formas de quebrar a dita cuja era com um ráio cósmico passando pelo HD e mexendo em bits. E o pior é que ele ficou chatedo quando a sala toda começou a rir!_
