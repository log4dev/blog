---
author: mgalves
categories:
- Design
comments: true
date: "2008-03-06T17:40:44Z"
slug: design-e-simplicidade
theme:
  name: twitter
title: Design e simplicidade
wordpress_id: 258
---

Venho por meio desta apresentar apenas o link de um post que eu encontrei no reddit e que sintetiza perfeitamente a questão do design de interfaces, e do design de soluções em geral:

[http://stuffthathappens.com/blog/2008/03/05/simplicity/ ](http://stuffthathappens.com/blog/2008/03/05/simplicity/)

Aliás, o post acima descreve em 3 imagens o que eu me matei para tentar explicar de forma bem meia boca no post [Design de Interfaces](http://log4dev.com/2007/09/23/design-de-interfaces/).
