---
author: mgalves
comments: true
date: "2007-10-12T12:21:16Z"
slug: sftp-e-scp-no-mac
tags:
- Apple
- Ferramentas
- Mac OS
- Redes
- Segurança
theme:
  name: twitter
title: sFTP e SCP no Mac
wordpress_id: 194
---

O **Mac** tem uma longa tradição de interfaces gráficas de alto nível. Mas como bom hacker e amante de Linux para trabalho, eu gosto muito de usar o Terminal para executar comandos como **SSH**, **sFTP** ou **SCP**. Apesar disso, confesso que as vezes  um programa gráfico torna as coisas mais confortáveis.  O [Fugu (http://rsug.itd.umich.edu/software/fugu/)](http://rsug.itd.umich.edu/software/fugu/) é um ótimo **frontend** para estas aplicações. Existem versões compatíveis com **Mac OS 10.2** até **Mac OS 10.4**
