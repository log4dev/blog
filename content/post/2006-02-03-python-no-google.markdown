---
author: mgalves
categories:
- Desenvolvimento
comments: true
date: "2006-02-03T09:43:56Z"
slug: python-no-google
tags:
- Python
- Google
theme:
  name: twitter
title: Python no Google
wordpress_id: 45
---

Esse [post](http://panela.blog-city.com/python_at_google_greg_stein__sdforum.htm) faz um pequeno resumo de uma paletra no SDForun de um engenheiro da Google, [Greg Stein](http://www.lyra.org/greg/), que falou sobre o uso de Python dentro da própria Google. É interessante para matar um pouco a curiosidade de como as coisas funcionam por lá....
