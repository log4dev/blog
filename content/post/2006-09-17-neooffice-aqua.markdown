---
author: mgalves
comments: true
date: "2006-09-17T12:26:31Z"
slug: neooffice-aqua
tags:
- Ferramentas
- Mac OS
theme:
  name: twitter
title: NeoOffice Aqua
wordpress_id: 81
---

A versão Aqua do NeoOffice já está disponível para download. Pra quem não sabe, o NeoOffice é um porte do pacote OpenOffice nativo para o Mac OS X. O OpenOffice tem um porte oficial para Mac, mas depende do uso do servidor X pra funcionar. O endereço para download do NeoOffice é  [http://www.neooffice.org/.](http://www.neooffice.org/)
