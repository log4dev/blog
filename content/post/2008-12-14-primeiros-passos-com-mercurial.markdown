---
author: mgalves
categories:
- Ferramentas
comments: true
date: "2008-12-14T21:25:31Z"
slug: primeiros-passos-com-mercurial
tags:
- Mercurial
theme:
  name: twitter
title: Primeiros passos com Mercurial
wordpress_id: 377
---

Instalei o [Mercurial](http://www.selenic.com/mercurial/wiki/) usando [MacPorts](http://www.macports.org/): _sudo port install mercurial_ (apenas para Mac).

Li rapidamente o [QuickInstall](http://www.selenic.com/mercurial/wiki/index.cgi/QuickStart) para aprender como baixar um repositório via ssh.

Clonei o repositório remoto que está no servidor do [Job4Dev](http://job4dev.com) (sim, a nova versão do Job4Dev está usando Mercurial como ferramenta de controle de versão. Antes usávamos SVN).

Agora realmente preciso ler o manual do dito cujo.

Aguardem o próximo capítulo.
