---
author: mgalves
categories:
- Notícias
comments: true
date: "2008-05-05T11:34:50Z"
slug: log4dev-no-webinsider-2-o-retorno
theme:
  name: twitter
title: Log4Dev no Webinsider 2 - O Retorno
wordpress_id: 293
---

Ele conseguiu de novo! Raphael teve o seu segundo artigo publicado no **Webinsider**:[ Como financiar a produção onde o consumo é livre?](http://webinsider.uol.com.br/index.php/2008/05/05/como-financiar-a-producao-onde-o-consumo-e-livre/)

Parabéns de novo!

E estou vendo que em breve  o log4dev será pequeno para ele. Aguardem: vem aí o lullis.net :-)
