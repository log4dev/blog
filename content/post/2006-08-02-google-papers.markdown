---
author: mgalves
comments: true
date: "2006-08-02T10:42:00Z"
slug: google-papers
tags:
- Formação
- Pesquisa
- Google
theme:
  name: twitter
title: Google Papers
wordpress_id: 74
---

Hoje, lendo meus RSSs, descobri a página Google Papers, que contém links para artigos escritos por pesquisadores e engenheiros que trabalham lá. Ainda não tive tempo de ler com calma os artigos (mesmo porque são muitos), mas tenho a impressão de que pode ser uma fonte interessante de informações.

[http://labs.google.com/papers/](http://labs.google.com/papers/)

Comentários específicos sobre papers e indicações são bem vindos.
