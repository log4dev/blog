---
author: mgalves
categories:
- Notícias
comments: true
date: "2008-11-07T08:28:32Z"
slug: speedy-agora-realmente-nao-pode-mais
tags:
- Speedy
- Web
theme:
  name: twitter
title: Speedy agora REALMENTE não pode mais
wordpress_id: 372
---

Há um tempo atrás, eu comentei da possibilidade do Speedy **"não poder mais"** oferecer autenticação sem a necessidade de provedor externo. Pois é: o dia chegou!  A partir de hoje, dia **07/11/2008**, a Telefônica, **por decisão judicial**, não pode mais oferecer autenticação através do usuário **internet@speedy.com.br**, e os usuários terão que contratar provedor externo.

Novamente, destaquei o "por decisão judicial", porque acho o cúmulo do absurdo o comunicado da empresa deixar  a entender que a culpa é da Justiça, eque está fazendo isso contra a sua vontade. Afinal todos sabemos que o inicio disso tudo foi uma liminar de um juiz de Baurú proibindo a Telfônica de exigir o provedor.

Ridículo.

A boa notícia é que o [site deles](http://www.speedyvantagens.com.br/provedores/) oferece uma lista de provedores com "preços e promoções" especiais.....
