---
author: mgalves
categories:
- Notícias
comments: true
date: "2008-02-06T22:43:21Z"
slug: preview-no-job4dev
tags:
- Desenvolvimento
- Design
- job4dev
theme:
  name: twitter
title: Preview no Job4Dev
wordpress_id: 244
---

Mais uma singela porém importante novidade no [Job4Dev](http://job4dev.com): agora o sistema mostra um preview do anúncio para que o usuário possa validar o texto e a formatação produzida pelo markdown antes de confirmar a submissão.
