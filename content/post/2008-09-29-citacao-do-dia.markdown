---
author: mgalves
categories:
- Desenvolvimento
- Opinião
comments: true
date: "2008-09-29T11:11:59Z"
slug: citacao-do-dia
theme:
  name: twitter
title: Citação do dia
wordpress_id: 357
---

Lida no [Manifesto 37 Signals](http://37signals.com/24.html), versão original (que eu nunca tinha lido)


> My Cousin's Buddy is a Web Designer

Then let him do it.

The money you save by using your cousin's buddy is nothing compared with the cost in time and money required to undo his mistakes.



Simples. Perfeito.
