---
author: mgalves
comments: true
date: "2007-03-29T13:03:53Z"
slug: quebra-cabeca-em-c
tags:
- C
- Linguagens de Programação
- Programação
theme:
  name: twitter
title: Quebra-cabeça em C
wordpress_id: 119
---

Segue uma lista de perguntas-pegadinhas sobre C.

[http://www.gowrikumar.com/c/index.html](http://www.gowrikumar.com/c/index.html)

Útil para amantes de problemas de computação e programação em geral, e essencial para candidatos a grandes empresas de software multinacionais que prezam a qualidade de seus programadores.
