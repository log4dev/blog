---
author: mgalves
categories:
- Notícias
comments: true
date: "2008-05-02T11:40:13Z"
slug: job4devcombr-no-ar
tags:
- job4dev
- Web
theme:
  name: twitter
title: job4dev.com.br no ar!
wordpress_id: 291
---

Acabei de configurar o domínio [job4dev.com.br](http://job4dev.com.br), que irá funcionar em paralelo ao domínio job4dev.com. Com isso espero melhorar os resultados nas pesquisas Google (afinal, agora temos um domínio genuinamente brasuca). Ajudem a divulgar a nova marca!
