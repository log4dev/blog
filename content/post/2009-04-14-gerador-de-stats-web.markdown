---
author: mgalves
comments: true
date: "2009-04-14T09:55:29Z"
slug: gerador-de-stats-web
tags:
- Ferramentas
- Web
theme:
  name: twitter
title: 'Gerador de Stats Web '
wordpress_id: 534
---

Pergunta à comunidade Log4Dev: alguém aí conhece um bom sistema de geração de estatísticas web a partir de log do Apache, de preferência de código aberto?
