---
author: mgalves
categories:
- Desenvolvimento
comments: true
date: "2006-11-23T18:29:32Z"
slug: cvs-x-subversion
tags:
- Ferramentas
- SVN
- CVS
- Desenvolvimento
theme:
  name: twitter
title: CVS X Subversion
wordpress_id: 95
---

Sempre tive curiosidade de saber as reais **diferenças** entre **CVS** e **Subversion**. Usei os dois (muito mais o primeiro do que o segundo), e fora algumas diferenças óbvias (tipo o fato que Subversion versiona pastas e metadados), não sabia bem os detalhes. Só sabia que todos diziam que Subversion é bem superior. Achei um [texto ](http://osdir.com/Article203.phtml)bastante interessante sobre este assunto. Não sei se é completo, mas fornece uma boa visão de ambos.
