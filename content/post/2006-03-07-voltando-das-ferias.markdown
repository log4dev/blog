---
author: mgalves
categories:
- Desenvolvimento
- Negocios
comments: true
date: "2006-03-07T18:11:13Z"
slug: voltando-das-ferias
theme:
  name: twitter
title: Voltando das férias
wordpress_id: 48
---

Após 3 semanas de férias, ainda não entrei no ritmo de trabalho e meu lado geek tá meio fraco e enferrujado :-) Mas só pra retomar o hábito de escrever aqui, segue o link de um artigo que eu achei interessante do [Joel Spolsky](http://www.joelonsoftware.com/) sobre o problema de precificar softwares. Ele trás bons elementos para pensar no problema. Provavelmente quem já fez um curso de economia, mercado e coisas correlatas irá achar inútil. Mas eu não fiz nenhum curso desses....ou melhor, fiz, mas não de uma forma que pudesse render frutos mais tarde. If you know what I mean.Para ler o artigo, clique [**aqui**](http://www.joelonsoftware.com/printerFriendly/articles/CamelsandRubberDuckies.html)
