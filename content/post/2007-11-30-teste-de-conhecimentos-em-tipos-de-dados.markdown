---
author: mgalves
categories:
- Desenvolvimento
comments: true
date: "2007-11-30T12:15:55Z"
slug: teste-de-conhecimentos-em-tipos-de-dados
tags:
- Formação
theme:
  name: twitter
title: Teste de conhecimentos em tipos de dados
wordpress_id: 216
---

Achei este [teste](http://www.infocider.com/blogs/index.php/programatic/?title=data_literacy_test_from_code_complete&more=1&c=1&tb=1&pb=1) no blog [Programatic](http://www.infocider.com/blogs/index.php/programatic/) que achei divertido. O autor do teste considera que pra escrever um bom programa, você deve saber quais as boas estruturas e tipos de dados devem ser usados. Concordo plenamente.

O teste é simples. Basicamente, uma lista de tipos de dados e estruturas. Faça a contagem de quantos você conhece, e verifique o seu nível de conhecimentos em programação.
