---
author: mgalves
comments: true
date: "2007-07-17T10:41:11Z"
slug: posicionamento-de-divs
tags:
- Web
- CSS
theme:
  name: twitter
title: Posicionamento de DIVs
wordpress_id: 153
---

O site [ Learn CSS Positioning in Ten Steps](http://www.barelyfitz.com/screencast/html-training/css/positioning/) mostra de uma forma muito simple, eficiente e prática como os diferentes modos de** posicionamento de DIVs** em uma **página web** com **CSS**. O interessante é que do lado direito da página, existe um pequeno demo que vai exemplificando os resultados obtidos.
