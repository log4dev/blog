---
author: mgalves
categories:
- Negocios
- Notícias
- Produto
comments: true
date: "2010-03-25T00:01:44Z"
slug: sigaseutime-na-midia
theme:
  name: twitter
title: SigaSeuTime na mídia
wordpress_id: 734
---

Faço minhas [as palavras de Lullis](http://log4dev.com/2010/02/01/porque-ando-tao-calado/): ando calado porque ando ultra atolado de coisas, e sem tempo para o ócio criativo. Muitas coisas estão acontecendo tanto no **SigaSeuTime** e no **Job4Dev**, e a boa notícia é esses acontecimentos estão sendo acompanhados pelo reconhecimento.

Hoje, o SigaSeuTime foi tema de uma matéria no site de **[Economia & Negócios do Estadão](http://economia.estadao.com.br/)**, intitulada **["Redes sociais dão oportunidade de negócio ao pequeno empreendedor"](http://economia.estadao.com.br/noticias/not_10591.htm)**. A matéria usa o projeto como case de como redes sociais como o Twitter servem de motor para novos empreendimentos e idéias.
