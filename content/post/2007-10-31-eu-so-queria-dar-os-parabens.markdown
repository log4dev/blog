---
author: lullis
comments: true
date: "2007-10-31T19:31:50Z"
slug: eu-so-queria-dar-os-parabens
tags: null
theme:
  name: twitter
title: Eu só queria dar os parabéns…
wordpress_id: 203
---

aos colabadores e leitores assíduos do blog. O mês está encerrando, e o blog passou a marca de 6000 acessos em Outubro.

Pode parecer pouco, mas 200 acessos diários em um blog que se dispõe a buscar um público tão pequeno (desenvolvedores e pessoas que trabalham com tecnologia), que se mantém escrevendo em português, e que não se preocupa em fazer marola, primando a qualidade, é uma marca que impressiona.

Próxima meta: 10 mil acessos.

Cheers.
