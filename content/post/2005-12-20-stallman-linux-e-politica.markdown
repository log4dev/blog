---
author: mgalves
categories:
- Negocios
comments: true
date: "2005-12-20T14:20:00Z"
slug: stallman-linux-e-politica
tags:
- Código Aberto
- Linux
theme:
  name: twitter
title: Stallman, Linux e Politica
wordpress_id: 19
---

A ZNet publicou uma entrevista muito interessante com Richard Stallman, onde ele fala sobre o movimento Software Livre, sobre o sistema Linux (ou melhor, GNU/Linux) e sobre  política. Longo, mas bastante interessante. Vale ressaltar a explicação entre as diferenças dos movimentos de Open Source e Free Software, e a menção à  UNIVATES e ao SAGU.
[
Clique aqui para ler o artigo.](http://www.zmag.org/content/showarticle.cfm?SectionID=13&ItemID=9350)

PS: Agradeço ao Danilo, que me indicou o texto.
