---
author: mgalves
categories:
- Humor
comments: true
date: "2007-09-11T10:27:58Z"
slug: porque-nao-usar-emacs-segundo-um-amante-de-vi
tags:
- Ferramentas
- Interfaces
theme:
  name: twitter
title: Porque não usar Emacs, segundo um amante de Vi
wordpress_id: 183
---

<div style="text-align: center;" markdown="1">
    ![](/images/2007-09-11-porque-nao-usar-emacs-segundo-um-amante-de-vi/xuf010710.gif)
</div>

PS: Esta charge, apesar de engraçada, vai contra a crença do autor de post de que [**Emacs**](http://www.gnu.org/software/emacs/) é muuuuuuuuuito melhor do que **Vi**.
