---
author: mgalves
categories:
- Notícias
comments: true
date: "2008-05-27T13:55:25Z"
slug: lullis-em-brasilia
theme:
  name: twitter
title: Lullis em Brasília
wordpress_id: 304
---

Que o Raphael virou pop, a gente já sabe: emplacou 3 ótimos artigos no Webinsider. Mas agora ele está virando referência nacional, citado pelo [MinC ](http://www.cultura.gov.br/site/?p=11805)e pela [Presidência da República](http://www.presidencia.gov.br/estrutura_presidencia/Subsecretaria/noticias/clipping/noticias/assunto9/nt07nov3g/).

Em breve, **log4dev **será rebatizado de **lullis4dev**, e o editor-chefe será enviado à Sibéria para cobrir a produção de software local. Com passagem só de ida...

:-(
