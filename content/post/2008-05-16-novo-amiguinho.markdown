---
author: mgalves
categories:
- Notícias
comments: true
date: "2008-05-16T00:09:17Z"
slug: novo-amiguinho
theme:
  name: twitter
title: Novo amiguinho
wordpress_id: 300
---

O Job4Dev tem um novo colaborador! Falem oi para o Thiago "Bart" Bartolomei.

Bart, seja bem vindo. Por favor, não se esqueça de preencher os 4 formulários de inscrição, fazer os exames de corpo delito e mandar a certidão de ficha policial limpa.  A pauta do blog é fechada todos os dias às 16h, todos os textos devem ser enviados imperativamente até às 20h e devem conter entre 500 e 502 palavras.

É muito divertido!
