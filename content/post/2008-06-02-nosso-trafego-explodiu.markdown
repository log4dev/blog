---
author: lullis
categories:
- Notícias
comments: true
date: "2008-06-02T15:52:08Z"
slug: nosso-trafego-explodiu
theme:
  name: twitter
title: Nosso tráfego explodiu!!
wordpress_id: 307
---

O tráfego, o cabo de rede, as paredes do rack do computador... tudo foi parar nas alturas!

Infelizmente, não estou falando em sentido figurado. [Houve uma explosão no datacenter do ThePlanet](http://www.allheadlinenews.com/articles/7011131199), por azar o datacenter que é usado pelo [webfaction](http://www.webfaction.com), que por azar é o serviço de hosting onde esse blog se encontra.

O esquisito da coisa é que os nossos outros sites (o [Job4Dev](http://job4dev.com.br) e o [Books4Dev](http://books4dev.com)) estão funcionando bem. Talvez seja um problema de propagação do DNS por parte das máquinas de redundância deles.

A ver.
