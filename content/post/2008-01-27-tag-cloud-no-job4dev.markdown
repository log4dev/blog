---
author: mgalves
categories:
- Design
- Notícias
comments: true
date: "2008-01-27T09:32:58Z"
slug: tag-cloud-no-job4dev
tags:
- Ferramentas
- Interface
- job4dev
- Web
theme:
  name: twitter
title: Tag cloud no Job4Dev
wordpress_id: 238
---

Atendendo ao clamor popular, coloquei no ar ontem um sistema de tag cloud no [Job4Dev](http://job4dev.com).

A primeira versão está bem simples: lista de tags ordenada por nome, com o número de anúncios em que a tag aparece do lado. A próxima versão, que deve sair em breve, será del.icio.us like, diferenciando as mais populares das menos populares pelo tamanho da fonte.

De grão em grão...
