---
author: mgalves
categories:
- Humor
comments: true
date: "2007-03-02T20:14:07Z"
slug: burlando-o-castigo
tags:
- Linguagens de Programação
- Programação
- Humor
theme:
  name: twitter
title: Burlando o castigo…
wordpress_id: 112
---

..ou "Diferentes formas de se simplificar a tarefa de escrever 500 vezes na lousa 'Nunca mais irei jogar aviões de papel nos meus colegas' quando se é um geek".

[http://www.jeffpalm.com/fox/index.html](http://www.jeffpalm.com/fox/index.html)
