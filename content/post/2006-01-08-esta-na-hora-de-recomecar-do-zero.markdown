---
author: mgalves
categories:
- Desenvolvimento
- Engenharia de Software
comments: true
date: "2006-01-08T22:37:00Z"
slug: esta-na-hora-de-recomecar-do-zero
theme:
  name: twitter
title: Esta na hora de recomeçar do zero ?
wordpress_id: 30
---

quem mexe com desenvolvimento pesado de software (e talvez nem tão pesado assim) provavelmente já passou pelo dilema de recomeçar ou não o desenvolvimento de um programa do zero, ou seja: reescrever completamente (ou quase) o código. E talvez o maior problema seja convencer seu superior que essa decisão em alguns casos é necessária, se não vital.

Eu já passei por esse dilema, e acabei optando pelo redesign de uma interface gráfica, cujas versões iniciais não tinham sido feitas por mim, e que estava sob minha responsabilidade. O código estava bem mal comentado, bem complexo de entender, e adicionar qualquer funcionalidade era um processo complicado. Após um mês tinha reescrito tudo, com uma arquitetura bem simples, baseada em patterns, que me permitia ser muito mais eficiente nas atualizações.

Leonardo me enviou este texto hoje, que dá um ponto de vista bem claro sobre situações nas quais o redesign é essencial. Curto e objetivo.

[http://www.jroller.com/page/bloritsch?entry=when_is_it_ok_to](http://www.jroller.com/page/bloritsch?entry=when_is_it_ok_to)
