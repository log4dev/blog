---
author: mgalves
categories:
- Desenvolvimento
- Humor
comments: true
date: "2008-03-20T10:41:52Z"
slug: frase-do-dia-4
tags:
- Linguagens de Programação
theme:
  name: twitter
title: Frase do Dia
wordpress_id: 266
---

> “There are only two kinds of languages: the ones people complain about and the ones nobody uses."


-[Bjarne Stroustrup](http://en.wikipedia.org/wiki/Bjarne_Stroustrup)(Criador do C++)
