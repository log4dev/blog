---
author: mgalves
comments: true
date: "2007-01-08T12:25:25Z"
slug: pendrive-linux
tags:
- Hardware
- Linux
theme:
  name: twitter
title: Pendrive Linux
wordpress_id: 101
---

Rodar **Linux **sem necessidade de instalar o sistema no HD do computador não é novidade: os CDs "bootaveis" são uma mão na roda e funcionam muito bem. O problema é que arquivos tinham que ser gravados no HD.

O [pendrive da Mandriva](http://www.mandriva.com/pt_br/linux/2007/node_3481) resolve isso. O produto é um pendrive de 2GB com a disto linux Mandriva instalado, que permite o boot direto à partir do dispositivo, e tem um sistema de arquivos formatado que permite gravar os dados.
