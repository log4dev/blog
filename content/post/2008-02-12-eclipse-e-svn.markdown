---
author: mgalves
categories:
- Desenvolvimento
comments: true
date: "2008-02-12T07:51:43Z"
slug: eclipse-e-svn
tags:
- Eclipse
- Ferramentas
- SVN
theme:
  name: twitter
title: Eclipse e SVN
wordpress_id: 247
---

**Jogo rápido**: caso seu ambiente de desenvolvimento inclua o [Eclipse ](http://www.eclipse.org)e [SVN](http://subversion.tigris.org/), aconselho fortemente o uso do plugin [Subversive ](http://www.polarion.org/index.php?page=overview&project=subversive)para gerenciamento de repositórios dentro da IDE. A experiência aqui no projeto onde eu trabalho foi que este plugin se mostrou muito mais estável e prático de trabalhar do que o [Subclipe](http://subclipse.tigris.org/).
