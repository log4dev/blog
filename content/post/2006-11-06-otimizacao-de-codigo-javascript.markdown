---
author: mgalves
categories:
- Desenvolvimento
comments: true
date: "2006-11-06T16:29:29Z"
slug: otimizacao-de-codigo-javascript
tags:
- Javascript
- Linguagens de Programação
- Web
theme:
  name: twitter
title: Otimização de código javascript
wordpress_id: 93
---

O artigo [_Efficient Javascript_](http://dev.opera.com/articles/view/48/) apresenta várias dicas para otimizar códigos em javascript. Importantíssimo para quem desenvolve aplicações web que usam a scripts de forma intensiva.
