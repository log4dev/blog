---
author: mgalves
comments: true
date: "2007-05-17T12:00:16Z"
slug: webdevelopertool-para-ie
tags:
- Ferramentas
- Interfaces
- Sistemas Operacionais
- Web
- Windows
- Ajax
theme:
  name: twitter
title: WebDeveloperTool para IE
wordpress_id: 135
---

Baixei hoje para a minha máquina a **versão 1.0** do **Internet Explorer Developer Toolbar**. Parece ser bastante útil para **depuração de interfaces** em **aplicações web**. Pra ficar completo, em tempos de AJAX, falta um bom analisador de requisições assíncronas estilo FireBug.

Mais informações podem ser encontradas [aqui](http://www.microsoft.com/downloads/details.aspx?FamilyId=E59C3964-672D-4511-BB3E-2D5E1DB91038&displaylang=en).
