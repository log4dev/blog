---
author: mgalves
categories:
- Desenvolvimento
comments: true
date: "2008-02-06T11:31:19Z"
slug: links-sobre-regex
tags:
- Javascript
- Regex
theme:
  name: twitter
title: Links sobre Regex
wordpress_id: 243
---

Para quem ainda tem dúvidas sobre a utilidade de Regex, dois links do mesmo blog:

[10 Reasons to Learn and Use Regular Expressions](http://blog.stevenlevithan.com/archives/10-reasons-to-learn-and-use-regular-expressions)

e
[Levels of JavaScript Regex Knowledge](http://blog.stevenlevithan.com/archives/levels-of-javascript-regex-knowledge)
