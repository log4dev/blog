---
author: mgalves
comments: true
date: "2006-05-25T17:51:46Z"
slug: apple-store-5th-avenue-new-york
tags:
- Apple
theme:
  name: twitter
title: Apple Store - 5th avenue, New York
wordpress_id: 65
---

As imagens falam por si só...o design dessa nova loja da Apple é um tanto quanto impressionante. Pelo menos a entrada !

[http://www.apple.com/retail/fifthavenue/gallery/index.html ](http://www.apple.com/retail/fifthavenue/gallery/index.html)

Um dia ainda vou visitar (daí aproveito e visito a Big Apple também...)
